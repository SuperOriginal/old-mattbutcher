package me.supercube101.mattbutcher;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

/**
 * Created by Aaron.
 */
public class main extends JavaPlugin{

    ArrayList<EntityType> badmobs = new ArrayList<EntityType>();

    public void onEnable(){
        badmobs.add(EntityType.BLAZE);
        badmobs.add(EntityType.CREEPER);
        badmobs.add(EntityType.GHAST);
        badmobs.add(EntityType.MAGMA_CUBE);
        badmobs.add(EntityType.SILVERFISH);
        badmobs.add(EntityType.SKELETON);
        badmobs.add(EntityType.SLIME);
        badmobs.add(EntityType.CAVE_SPIDER);
        badmobs.add(EntityType.ENDERMAN);
        badmobs.add(EntityType.SPIDER);
        badmobs.add(EntityType.PIG_ZOMBIE);
        badmobs.add(EntityType.WITCH);
        badmobs.add(EntityType.ZOMBIE);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){

        if(cmd.getName().equalsIgnoreCase("mbutcher")){
            if(sender instanceof Player){
                sender.sendMessage(ChatColor.RED + "Only console can use this command!");
                return true;
            }

            if(args.length == 0){
                sender.sendMessage("Usage: /mbutcher <worldname>");
                return true;
            }else if(args.length >= 1){
                World world = Bukkit.getServer().getWorld(args[0]);
                if(world == null){sender.sendMessage("That world does not exist!"); return true;}
                else{
                    int i = 0;
                    for(Entity entity:Bukkit.getServer().getWorld(args[0]).getEntities()){

                        if(badmobs.contains(entity.getType())){
                            entity.remove();
                            i++;
                        }

                    }
                    sender.sendMessage("Successfully removed " + i + " entities from world " + args[0] + ".");
                }

            }

        }



    return true;
    }


}
